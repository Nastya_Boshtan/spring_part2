package beans1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private String name;
//    public BeanB(String name){
//        this.name=name;
//    }
//    public String getValue() {
//        return name;
//    }
    @Value("BeanB")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name=" + name +
                '}';
    }

}
