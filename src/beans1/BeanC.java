package beans1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    private String name;
//    public BeanC(String name){
//        this.name=name;
//    }
//    public String getValue() {
//        return name;
//    }
    @Value("BeanC")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name=" + name +
                '}';
    }

}
