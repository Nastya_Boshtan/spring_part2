package InterfaceBeans;

import DependencyInjection.Beans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class PrimBean {
@Autowired
Beans bean1;

@Autowired
    @Qualifier("firstBean")
    Beans bean2;
    @Autowired
    @Qualifier("thirdBean")
    Beans bean3;

    public String toString(){
        return "bean1 "+ bean1.getBeans()+ " bean2 "+bean2.getBeans()+" bean3 "+bean3.getBeans();
    }

}
