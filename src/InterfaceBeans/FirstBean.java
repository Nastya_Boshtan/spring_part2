package InterfaceBeans;

import DependencyInjection.Beans;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Order(2)
public class FirstBean implements Beans {
    //private String name;
    @Override
    //@Value("FirstBean")
    public String getBeans() {
        return "FirstBean";
    }
}
