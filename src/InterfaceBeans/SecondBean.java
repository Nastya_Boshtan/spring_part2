package InterfaceBeans;

import DependencyInjection.Beans;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Primary
public class SecondBean implements Beans {
    //private Beans name;
    @Override
    //@Value("SecondBean")
    public String getBeans() {
        return "SecondBeans";
    }
}
