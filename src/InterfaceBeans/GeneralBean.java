package InterfaceBeans;

import DependencyInjection.Beans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneralBean {
    @Autowired
    private List<Beans> beans;

    public void printBeans(){
        for(Beans bean:beans){
            System.out.println(bean.getBeans());
        }
    }
}
