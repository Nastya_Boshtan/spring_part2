package InterfaceBeans;

import DependencyInjection.Beans;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class ThirdBean implements Beans {
    //private Beans name;
    @Override
    //@Value("ThirdBean")
    public String getBeans() {
        return "ThirdBeans";
    }
}
