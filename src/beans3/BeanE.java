package beans3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanE {
    private String name;

    @Value("BeanE")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name=" + name +
                '}';
    }
}
