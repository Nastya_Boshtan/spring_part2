package beans3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanF {
    private String name;

    @Value("BeanF")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name=" + name +
                '}';
    }
}
