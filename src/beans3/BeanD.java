package beans3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private String name;

    @Value("BeanD")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name=" + name +
                '}';
    }
}
