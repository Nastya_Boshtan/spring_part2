package beans2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    private String name;

    @Value("RoseFlower")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "name=" + name +
                '}';
    }
}
