package beans2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
    private String name;

    @Value("NarcissusFlower")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "name=" + name +
                '}';
    }

}
