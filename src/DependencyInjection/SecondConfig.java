package DependencyInjection;

import beans2.CatAnimal;
import beans3.BeanD;
import beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages ="beans2", /*includeFilters = @ComponentScan.Filter(type=FilterType.REGEX,pattern = "bens2\\.\\w+Flower"),*/excludeFilters = @ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE,classes = CatAnimal.class))
@ComponentScan(basePackageClasses = {BeanD.class,BeanF.class})
@ComponentScan(basePackages = "OtherBeans")
public class SecondConfig {

}
