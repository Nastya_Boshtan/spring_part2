package DependencyInjection;

import InterfaceBeans.GeneralBean;
import InterfaceBeans.PrimBean;
import OtherBeans.OtherBeanA;
import OtherBeans.OtherBeanB;
import OtherBeans.OtherBeanC;
import Profile.MedicineConfig;
import Profile.Medicines;
import Profile.MyMedicine;
import beans1.BeanA;
import beans1.BeanB;
import beans1.BeanC;
import beans2.CatAnimal;
import beans2.NarcissusFlower;
import beans2.RoseFlower;
import beans3.BeanD;
import beans3.BeanE;
import beans3.BeanF;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(String[] args){
        ApplicationContext context = new AnnotationConfigApplicationContext(FirstConfig.class,SecondConfig.class, Config.class);
       System.out.println(context.getBean(BeanA.class));
        System.out.println(context.getBean(BeanB.class));
        System.out.println(context.getBean(BeanC.class));
        //System.out.println(context.getBean(CatAnimal.class));
        System.out.println(context.getBean(NarcissusFlower.class));
        System.out.println(context.getBean(RoseFlower.class));
        System.out.println(context.getBean(BeanD.class));
        //System.out.println(context.getBean(BeanE.class));
        System.out.println(context.getBean(BeanF.class));
        //System.out.println(context.getBean(OtherBeanA.class));
        System.out.println("Hashcode OtherBeanA");
        System.out.println(context.getBean(OtherBeanA.class).hashCode());
        System.out.println(context.getBean(OtherBeanA.class).hashCode());
        System.out.println("Hashcode OtherBeanB");
        System.out.println(context.getBean(OtherBeanB.class).hashCode());
        System.out.println(context.getBean(OtherBeanB.class).hashCode());
        System.out.println("Hashcode OtherBeanC");
        System.out.println(context.getBean(OtherBeanC.class).hashCode());
        System.out.println(context.getBean(OtherBeanC.class).hashCode());
        context.getBean(GeneralBean.class).printBeans();
        System.out.println(context.getBean(PrimBean.class));

        AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext();
        context2.getEnvironment().setActiveProfiles("dev");
        context2.register(MedicineConfig.class);
        context2.refresh();
        System.out.println(context2.getBean(Medicines.class));
        System.out.println(context2.getBean(MyMedicine.class));

        AnnotationConfigApplicationContext context3 = new AnnotationConfigApplicationContext();
        context3.getEnvironment().setActiveProfiles("prod");
        context3.register(MedicineConfig.class);
        context3.refresh();
        System.out.println(context3.getBean(Medicines.class));
        System.out.println(context3.getBean(MyMedicine.class));





    }
}
