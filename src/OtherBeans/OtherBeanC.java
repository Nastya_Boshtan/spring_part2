package OtherBeans;

import beans1.BeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanC {
    private BeanB beanb;
@Autowired
@Qualifier("beanB")
public void setName(BeanB beanb) {
    this.beanb = beanb;
}
    @Override
    public String toString() {
        return "OtherBeanC{" +
                "beanb=" + beanb +
                '}';
    }
}
