package OtherBeans;

import beans1.BeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanA {

    private BeanA beanA;
    @Autowired

    public OtherBeanA(BeanA beanA){
        this.beanA=beanA;
    }
    @Override
    public String toString() {
        return "OtherBeanA{" +
                "beanA=" + beanA +
                '}';
    }

}
