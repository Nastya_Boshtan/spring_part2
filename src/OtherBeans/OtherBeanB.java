package OtherBeans;

import beans1.BeanA;
import beans1.BeanC;
import beans2.CatAnimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class OtherBeanB {
    @Autowired
    private BeanC beanC;
    @Override
    public String toString() {
        return "OtherBeanB{" +
                "beanC=" + beanC +
                '}';
    }

}
