package Profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class SecondMedicineComponent  extends AbstractMedicine{
    public SecondMedicineComponent(){
        name="Nimasul";
    }
    public String toString(){
        return "name " + name;
    }

}
