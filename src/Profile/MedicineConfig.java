package Profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("Profile")
public class MedicineConfig {
    @Bean
    @Profile("dev")
    public Medicines firstExample(){
        String name = "Analgin";
        return new Medicines(name);
    }

    @Bean
    @Profile("prod")
    public Medicines secondExample(){
        String name = "Afida";
        return new Medicines(name);
    }
}
