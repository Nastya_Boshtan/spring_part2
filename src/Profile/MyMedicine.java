package Profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyMedicine {
    @Autowired
    private AbstractMedicine medicines;

    public String toString(){
        return medicines.toString();
    }
}
