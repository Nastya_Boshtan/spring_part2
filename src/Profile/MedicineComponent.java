package Profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class MedicineComponent extends AbstractMedicine {
    public MedicineComponent(){
        name="Spazmalgon";
    }
    public String toString(){
        return "name " + name;
    }



}
